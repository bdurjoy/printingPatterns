import java.util.Scanner;

public class MyDemo {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i, j, k, signs;

		System.out.println("Enter any number between 5 to 20 to see the structure of the signs: ");
		signs = input.nextInt();

		for(i = 0; i < signs; i++) {
			for(k = (signs-1); k> i; k-- ) {
				System.out.print(" ");
			}
			for(j = 0; j<=i; j++) {

				System.out.print("$");
			}
			System.out.println();
		}

		System.out.println();

		for(i = 0; i < signs; i++) {
			for(j = 0; j<=i; j++) {

				System.out.print("$");
			}
			System.out.println();
		}


		System.out.println();

		for(i = 0; i < signs; i++) {
			for(k = 0; k < i; k++ ) {
				System.out.print(" ");
			}
			for(j = signs; j > i; j--) {

				System.out.print("$");
			}
			System.out.println();
		}


		System.out.println();

		for(i = 0; i < signs; i++) {
			for(j = signs; j > i; j--) {

				System.out.print("$");
			}
			System.out.println();
		}
	}
}